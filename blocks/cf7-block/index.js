const { __ } = wp.i18n;
const {
	registerBlockType,
} = wp.blocks;

import Editor from './Editor';
import './style.scss';

import './store.js';

registerBlockType( 'koolkat/contact-form-7', {
	title: __( 'Contact Form 7' ),
	icon: 'index-card',
	category: 'common',
	supports: {
		html: false,
	},
	edit: Editor,
	attributes: {
		formID: {
			type:Number,
		},
		title: {
			type:Number,
		},
	},
	save() {
		return null;
	},
} );
