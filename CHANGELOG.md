 ### v1.1.2 - 2020-01-22
  **Changes:** 
  *add attributes of formID to main javascript file to keep block selection in editor.
 ### v1.1.1 - 2020-01-22
  **Changes:** 
  *restructure to follow Zac Gordon's block development recommendations.
 ### v1.0.0 - 2018-10-08 
 **Changes:** 
 * Fixed Gutenberg block
 
 ### v1.0.0 - 2018-01-18 
 **Changes:**
  * First release.
  
